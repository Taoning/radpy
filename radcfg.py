import ConfigParser
import os

def setup_dir():
    dirs = ['matrices', 'models', 'objects', 'octrees',
            'resources', 'results', 'sources','viewpoints']
    [os.makedir(i) for i in dirs]

def setup_cfg():
    config = ConfigParser.RawConfigParser()

    config.add_section('SimulationControl')
    config.set('SimulationControl', 'vmx_opt', '-ab 8 -ad 65000 -lw 1e-8')
    config.set('SimulationControl', 'fmx_opt', '-ab 5 -c 500 -lw 1e-4')
    config.set('SimulationControl', 'dmx_opt', '-ab 2 -c 5000')
    config.set('SimulationControl', 'view_ray_cnt', 9)
    config.set('SimulationControl', 'pixel_jitter', .7)
    config.set('SimulationControl', 'accurate', False)
    config.set('SimulationControl', 'nproc', 1)

    config.add_section('FileStructure')
    config.set('FileStructure', 'base_dir', os.path.abspath(os.getcwd()))
    config.set('FileStructure', 'matrices', 'matrices')
    config.set('FileStructure', 'results', 'result')
    config.set('FileStructure', 'objects', 'objects')
    config.set('FileStructure', 'viewpoints', 'viewpoints')
    config.set('FileStructure', 'models', 'models')
    config.set('FileStructure', 'resources', 'resources')

    config.add_section('Models')
    config.set('Models', 'material', 'material.rad')
    config.set('Models', 'model', 'room.rad')
    config.set('Models', 'geometry', ['obj1.rad', 'obj2.rad'])

    config.add_section('Viewpoints')
    config.set('Viewpoints', 'points', 'wpi.pts')
    config.set('Viewpoints', 'views', 'south.vf')

    config.add_section('SurfaceGrid')
    config.set('SurfaceGrid', 'surfaces', 'floor.rad')
    config.set('SurfaceGrid', 'distances', .8)
    config.set('SurfaceGrid', 'spacings', .6)

    config.add_section('Windows')
    config.set('Windows', 'window_group1', ['window1.rad, window2.rad'])
    config.set('Windows', 'window_group2', ['window3.rad'])

    config.add_section('ncp_shade')
    config.set('ncp_shade', 'material1', 'fabric.rad')
    config.set('ncp_shade', 'geometry1', 'awning.rad')
    config.set('ncp_shade', 'material2', 'concrete.rad')
    config.set('ncp_shade', 'geometry2', 'overhang.rad')

    config.add_section('bsdf')
    config.set('bsdf', 'vis', ['shade.xml','no_shade.xml'])
    config.set('bsdf', 'sol')
    config.set('bsdf', 'shgc')
    config.set('bsdf', 'mtx')

    config.add_section('weather')
    config.set('weather', 'smx')
    config.set('weather', 'wea')
    config.set('weather', 'epw')
    config.set('weather', 'lat')
    config.set('weather', 'lon')
    return config

def write_cfg(config):
    with open('radmtx.cfg', 'w') as cfg:
        config.write(cfg)
