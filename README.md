# Radiance additions (In Progress)
Python based addons to various Radiance runs
Most are individual program operating on its own, and they are intended to be run from terminal/command line. Follow the program name by -h to get help/instruction on how to use each program.

## epw2wea_m
modified epw2wea script with the added capability to include only daylight hours in the output
## wea2sky
calling Radiance gendaymtx to generate individual skyvector files
## evalglared
wrapper to the original evalglare with output in json format and parallel processing supported by python concurrent module
## evgdata
compile individual json file (each corresponds to a single image) into a tsv file
## imgop
image based matrices operation (wrapper to dctimestep/rmtxop/pcomb) for Radiance 2/3/4/5/6 phase methods
## genport
Automatically generate F port for constructing F matrix, given window and NCP rad file. (note: only works on rectangular coplanar windows)
## genmtx
Higher level abstraction of the Radiance rfluxmtx/rcontrib program. This guy needs util.py and vecop.py to work.
1. -s: --sender, sender file path. Sender, for example, can be a view file, point file, or a polygon surface;
2. -r: --receiver, receiver file path. Receiver, for example, can be the window surface or sky;
3. -env: --envrivonment. Scene file
4. -o, output file path, or directory for image based matrix
5. -ofs, surface offset of a polygon to its normal (usually the window surface when generating a view matrix)
6. -fopt, option file, a file that contains all simulation parameter
7. -opts, options string in quote; will supercede the option file setting
8. -xy, image resolution when generating image based matrix
## setupw
Suggesting simulation methods based on the user's response to a series of question, setup directories to help organize
## cookie_cutter
...Automatic...LEED...etc...
## gengrid
Generate grid points given radiance primitives (polygons for now)
## genmodel
Generate a side-lit shoe box model
# Example 
