#!/usr/bin/env python3
"""
Convert a .wea file to skyvector files
One file will be created per entry in .wea file

Taoning.W
"""

import argparse
import multiprocessing as mp
import os
import re
import subprocess as sp
from tempfile import mkdtemp
import pdb


def Execute(cmd):
    """High level execution function."""
    sp.call(cmd, shell=True)


class wea2sky(object):
    """Main .wea to sky class."""

    SKY_GRD = "\nskyfunc glow skyglow\n0\n0\n4 0.9 0.9 1 0\n\n"
    SKY_GRD += "skyfunc glow groundglow\n0\n0\n4 1 1 1 0\n\n"
    SKY_GRD += "skyglow source sky\n0\n0\n4 0 0 1 180\n\n"
    SKY_GRD += "groundglow source ground\n0\n0\n4 0 0 -1 180\n"

    def __init__(self,
                 mode,
                 wea_fpath,
                 out_dir,
                 grd_refl=None,
                 spect='vis',
                 mf=None,
                 direct=False):
        """Initialize the class."""
        if spect == 'vis':
            out_spect = '0'
        elif spect == 'sol':
            out_spect = '1'

        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        grd_refl = 0.2 if grd_refl is None else grd_refl

        if mode == 'discretize':
            if mf is None:
                print("defaulting mf to 4")
                mf = 4
            tmpd = mkdtemp()
            header, content = self.read_wea(wea_fpath)
            for line in content:
                line = line.strip().split()
                minute = round(60 * (float(line[2]) - int(float(line[2]))), 0)
                fname = "%02d%02d_%02d%02d" % (int(line[0]), int(
                    line[1]), int(float(line[2])), minute)
                head_str = ''.join(header[:])
                with open('%s/%s.wea' % (tmpd, fname), 'w') as writer:
                    writer.write(head_str)
                    writer.write(' '.join(line) + '\n')

            fpaths = [
                os.path.join(tmpd, i) for i in os.listdir(tmpd)
                if i.endswith(".wea")
            ]
            cmds = [
                self.gendaymtx(f, mf, out_spect, direct, out_dir)
                for f in fpaths
            ]
            print(cmds)

            nproc = mp.cpu_count()
            p = mp.Pool(nproc)
            p.map(Execute, cmds)

        elif mode == 'continuous':
            self.gendaylit(wea_fpath, out_dir, out_spect, grd_refl)

    def gendaymtx(self, wea, mf, spect, direct, out_dir):
        """Use gendaymtx to generate discretized skies."""
        ds = '' if direct is False else '-d'
        out_name = os.path.splitext(os.path.basename(wea))[0]
        cmd = "gendaymtx -of -m %s -O%s %s %s > %s/%s.smx" % (
            mf, spect, ds, wea, out_dir, out_name)
        return cmd

    def gendaylit(self, wea, out_dir, spect, grd_refl):
        """Call gendaylit to generate continouse skies."""
        header, content = self.read_wea(wea)
        lat = header[1].split()[1]
        lon = header[2].split()[1]
        mer = header[3].split()[1]

        for line in content:
            line = line.split()
            dni = float(line[3])
            dhi = float(line[4])
            minute = round(60 * (float(line[2]) - int(float(line[2]))), 0)
            fname = "%02d%02d_%02d%02d" % (int(line[0]), int(
                line[1]), int(float(line[2])), minute)
            dt_str = ' '.join(line[:3])
            cmd = "gendaylit %s -a %s -o %s -m %s -W %s %s -g %s -O %s"\
                % (dt_str, lat, lon, mer, dni, dhi, grd_refl, spect)
            process = sp.Popen(cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE)
            stdout, stderr = process.communicate()
            with open('%s/%s.rad' % (out_dir, fname), 'wb') as wt:
                wt.write(stdout)
            with open('%s/%s.rad' % (out_dir, fname), 'a') as wt:
                wt.write(self.SKY_GRD)

    def read_wea(self, wea_fpath):
        """Read a wea file and return its header and content."""
        with open(wea_fpath, 'r') as reader:
            lines = reader.readlines()

        for i in range(len(lines)):
            if re.match(r"^\d+.*$", lines[i]):
                sep_line = i
                break

        header = lines[:sep_line]
        content = lines[sep_line:]

        return header, content


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-m',
                        required=True,
                        help="mode of operation: (discretize | continuous)")
    parser.add_argument('wea_fpath', help="wea file path")
    parser.add_argument('out_dir', help="output directory")
    parser.add_argument('-s', help="output spectrum {vis | sol}")
    parser.add_argument('-mf', help="multiplication factor, default=4")
    parser.add_argument("-direct",
                        action="store_true",
                        help="producing sun only skies")
    parser.add_argument('-g', help='Ground reflectance')
    args = parser.parse_args()
    wea2sky(args.m,
            args.wea_fpath,
            args.out_dir,
            grd_refl=args.g,
            mf=args.mf,
            spect=args.s,
            direct=args.direct)
