#!/usr/bin/env python3
"""Converting bsdf data in idf format to Radiance format."""
#
# T.Wang
#

import os
from argparse import ArgumentParser
import radutil


class Idf2mtx:
    """Converting bsdf data in idf format to Radiance format."""

    def __init__(self, idf_path, out_dir):
        """Initialize the class with input IDF path and output directory."""
        fname = os.path.splitext(os.path.basename(
            idf_path))[0]
        idf_sections = radutil.parse_idf(idf_path)
        btdf_sections = idf_sections['matrix:twodimension']
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        for sec in btdf_sections:
            name = sec[0]
            output_path = os.path.join(out_dir, "%s_%s.mtx" % (fname, name))

            row_cnt = int(sec[1])
            col_cnt = int(sec[2])
            if sec[2] in radutil.BASIS_DICT:
                _btdf_data = self.nest_list(list(map(float, sec[3:])), col_cnt)
                lambdas = self.angle_basis_coeff(radutil.BASIS_DICT[sec[2]])
                sdata = [list(map(lambda x, y: x * y, i, lambdas)) for i in _btdf_data]
                with open(output_path, 'w') as wt:
                    header = '#?RADIANCE\nNCOMP=3\n'
                    header += 'NROWS=%d\nNCOLS=%d\n' % (row_cnt, col_cnt)
                    header += 'FORMAT=ascii\n\n'
                    wt.write(header)
                    for row in sdata:
                        for val in row:
                            string = '\t'.join(['%07.5f' % val] * 3)
                            wt.write(string)
                            wt.write('\t')
                        wt.write('\n')

    def angle_basis_coeff(self, basis):
        """Calculate klems basis coefficient."""
        ablist = radutil.ABASE_LIST[basis]
        lambdas = []
        for i in range(len(ablist)-1):
            tu = ablist[i+1][0]
            tl = ablist[i][0]
            np = ablist[i][1]
            for n in range(np):
                lambdas.append(radutil.lambda_calc(tl, tu, np))
        return lambdas

    def nest_list(self, li, col_cnt):
        """Make a list of list give the column count."""
        nested = []
        for i in range(0, len(li), col_cnt):
            sub_list = []
            for n in range(col_cnt):
                try:
                    sub_list.append(li[i+n])
                except IndexError:
                    break
            nested.append(sub_list)
        return nested


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-ie', help='idf file path', required=True)
    parser.add_argument('-o', default=os.getcwd(), help='output file path')
    args = parser.parse_args()
    Idf2mtx(args.ie, args.o)
