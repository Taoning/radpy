#!/usr/bin/env python3

import radutil
import configparser
from genmtx import Genmtx, Sender, Receiver
import multiprocessing as mp
import os
import subprocess as sp
import tempfile as tf
import pdb


def CHECKOUT(cmd):
    return sp.check_output(cmd, shell=True)


class rad_model:
    """."""
    to_1col = "rmtxop -fa -c "
    to_light = to_1col + "47.4 119.9 11.6"
    to_energy = to_1col + ".265 .67 .065"

    def __init__(self, cfg_path):
        '''
        Setup a Radiance model for dynamic simulation
        parameters:
        '''

        self.parse_config(cfg_path)
        pdb.set_trace()
        self.matrices_3ph()
        if self.sim_ctrl['direct'] == 'on':
            self.make_direct_matrices()
        if self.sim_ctrl['matrix_only'] == 'off':
            # multiply matrix
            pass

    def parse_config(self, cfg_path):
        _config = configparser.ConfigParser()
        _config.read(cfg_path)
        config = _config._sections
        self.config = config

        self.sim_ctrl = config['simulation_control']
        self.file_strct = config['file_structure']
        self.objects = self.file_strct['objects']
        self.model = config['model']
        self.climate = config['climate']
        self.raysenders = config['raysender']
        self.sensor_srfs = config['sensor_surfaces']
        self.ncp_shade = config['non_coplanar_shade']
        self.bsdf = config['bsdf']

    #def run(self,):
    #    if len(cfs) > 1:
    #        if direct:
    #            run 5phm
    #        else:
    #            run 3phm
    #    elif len(ncps) > 1:
    #        if direct:
    #            run 6phm
    #        else:
    #            run 4phm
    #    elif direct:
    #        run 2phmd
    #    else:
    #        run 2phm

    def get_raysenders(self):
        grid = os.path.join(self.file_strct['raysender'],
                            self.raysender['grid'])
        view = self.raysender['view']
        grid_srfs = self.sensor_srfs['surface']
        dist = self.sensor_srfs['dist']
        spacing = self.sensor_srfs['spacing']
        assert [grid, view, grid_srfs] != ['', '', '']
        if grid != '':
            with open(grid, 'r') as rdr:
                self.grid_str = rdr.read()
        elif grid_srfs != '':
            self.grid_str = radutil.gen_grid(grid_srfs, spacing, dist, op=True)
        if view != '':
            self.view_path = view

    def matrices_2phm(self):
        _obj = self.file_strct['objects']
        env = [self.model['material']]
        env += self.model['opaque_surfaces'].split(' ')
        env_path = [os.path.join(_obj, e) for e in env]
        if self.grid_str:
            sndr = Sender()
            rcvr = Receiver()
            outpath = os.path.join(self.file_strct['matrices'], 'grid.dcmx')
            Genmtx(sender=sndr,
                   receiver=rcvr,
                   out_path=outpath,
                   env=env_path,
                   opt=opt)
        if self.view_path:
            sndr = Sender()
            rcvr = Receiver()
            outpath = os.path.join(self.file_strct['matrices'], 'view')
            Genmtx(sender=sndr,
                   receiver=rcvr,
                   out_path=outpath,
                   env=env_path,
                   opt=opt)

    def group_window(self, window_prims):
        """Group window polygons by their normal."""
        window_norms = [str(p['polygon'].normal()) for p in window_prims]
        norm_set = {norm: [] for norm in window_norms}
        for prim, norm in zip(window_prims, window_norms):
            norm_set[norm].append(prim)
        return norm_set

    def matrices_3phm(self, fresh=False):
        """."""
        window_paths = [
            os.path.join(self.objects, g)
            for g in self.model['glass'].split(' ')
        ]
        window_prims = [radutil.parse_primitive(p) for p in window_paths]
        window_prims = [i for n in window_prims for i in n]
        if self.sim_ctrl['group_window_by_direction'] != 'off':
            windows = self.group_window(window_prims)
        else:
            windows = window_prims
        self.wpi_vmx = []
        self.ev_vmx = []
        self.dmxs = []
        self.window_areas = []
        for zone in range(self.wz):
            _zone = "{:02}".format(zone)
            window_name = self.window_names['window{}'.format(zone)]
            win_path = os.path.join(self.window_path, window_name)
            vmx_rcvr = Receiver(win_path, self.vsamp)
            wdow_name = util.basename(win_path)
            wdow_prim = util.parse_primitive(win_path)
            wdow_area = sum([p['polygon'].area() for p in wdow_prim])
            win_area = wdow_area * 0.00065  # sq inch -> sq meter
            self.window_areas.append(win_area)

            _wpi_name = self.wpi_name + '_' + _zone
            wpi_out = os.path.join(self.matrix_path, _wpi_name)
            self.wpi_vmx.append(wpi_out)

            _ev_name = self.ev_name + '_' + _zone
            ev_out = os.path.join(self.matrix_path, _ev_name)
            self.ev_vmx.append(ev_out)

            dname = '_'.join([wdow_name, 'sky_{}'.format(self.dsamp)])
            dmx_out = os.path.join(self.matrix_path, dname)
            self.dmxs.append(dmx_out)

            if fresh:
                vmx_args = {'env': self.env_path, 'opt_path': self.vopt}
                Genmtx(sender=self.wpi_sender,
                       receiver=vmx_rcvr,
                       out_path=wpi_out,
                       **vmx_args)
                Genmtx(sender=self.ev_sender,
                       receiver=vmx_rcvr,
                       out_path=ev_out,
                       **vmx_args)
                _dmx_sender = Sender(wdow_prim, sampling=self.vsamp)
                Genmtx(sender=_dmx_sender,
                       receiver=self.dmx_rcvr,
                       out_path=dmx_out,
                       env=self.env_path,
                       opt_path=self.dopt)
                print('done generating general')

    def matrices_3ph_direct(self, fresh=False):
        """."""
        vopt_d = os.path.join(self.opt_path, self.opt_names['vmx_d'])
        dopt_d = os.path.join(self.opt_path, self.opt_names['dmx_d'])
        wpi_d_name = self.wpi_name + '_d'
        ev_d_name = self.ev_name + '_d'
        self.wpi_vmx_d = []
        self.ev_vmx_d = []
        self.dmxs_d = []
        for zone in range(self.wz):
            _zone = "{:02}".format(zone)
            window_name = self.window_names['window{}'.format(zone)]
            win_path = os.path.join(self.window_path, window_name)

            _wpi_d_name = wpi_d_name + '_' + _zone
            wpi_out = os.path.join(self.matrix_path, _wpi_d_name)
            self.wpi_vmx_d.append(wpi_out)

            _ev_name = ev_d_name + '_' + _zone
            ev_out = os.path.join(self.matrix_path, _ev_name)
            self.ev_vmx_d.append(ev_out)

            dname = '_'.join([wdow_name, 'sky_d_{}'.format(self.dsamp)])
            dmx_out = os.path.join(self.matrix_path, dname)
            self.dmxs_d.append(dmx_out)

            if fresh:
                vmx_rcvr = Receiver(win_path, self.vsamp)
                vmx_args = {'env': self.env_black, 'opt_path': self.vopt_d}
                Genmtx(sender=self.wpi_sender,
                       receiver=vmx_rcvr,
                       out_path=wpi_out,
                       **vmx_args)
                Genmtx(sender=self.ev_sender,
                       receiver=vmx_rcvr,
                       out_path=ev_out,
                       **vmx_args)
                _dmx_sender = Sender(win_path, sampling=self.vsamp)
                Genmtx(sender=_dmx_sender,
                       receiver=self.dmx_rcvr,
                       out_path=dmx_out,
                       env=env_black,
                       opt_path=dopt_d)

    def genskyvec_cmd(self,
                      dni,
                      dhi,
                      month,
                      day,
                      hours,
                      direct=False,
                      solar=False,
                      onesun=False,
                      rotate=0):
        sun_only = ' -d' if direct else ''
        spect = '-O 1' if solar else '-O 0'
        _five = ' -5' if onesun else ''
        sky_cmd = "gendaylit {} {} {} -a {} -o {} -W {} {} {} ".format(
            month, day, hours, self.lat, self.lon, dni, dhi, spect)
        skv_cmd = "| genskyvec -m {}{}{} ".format(self.dsamp[-1], sun_only,
                                                  _five)
        rotate_cmd = "| xform -e -rz {} ".format(rotate)
        cmd = sky_cmd + rotate_cmd + skv_cmd
        return cmd

    def gendaymtx_cmd(self,
                      dni,
                      dhi,
                      month,
                      day,
                      hours,
                      direct=False,
                      solar=False,
                      onesun=False,
                      rotate=0):
        sun_only = ' -d' if direct else ''
        spect = ' -O1' if solar else ' -O0'
        _five = ' -5 .533' if onesun else ''
        wea_head = "place test\\nlatitude {}\\nlongitude {}\\n".format(
            self.lat, self.lon)
        wea_head += "time_zone 120\\nsite_elevation 100\\nweather_data_file_units 1\\n"
        wea_data = '{} {} {} {} {}'.format(month, day, hours, dni, dhi)
        wea_cmd = '!echo "{}{}"'.format(wea_head, wea_data)
        skv_cmd = " | gendaymtx -r {} -m {}{}{}{}".format(
            rotate, self.dsamp[-1], sun_only, _five, spect)
        cmd = wea_cmd + skv_cmd
        return cmd

    def compute(self, vmx, tmx, dmx, skv_cmd, solar=False):
        assert len(tmx) == self.wz
        conversion = self.to_energy if solar else self.to_light
        commands = []
        for zone in range(self.wz):
            _v = vmx[zone]
            _t = tmx[zone]
            _d = dmx[zone]
            cmd = "rmtxop {} {} {} \'{}\' | {} - | getinfo -".format(
                _v, _t, _d, skv_cmd, conversion)
            commands.append(cmd)
        result = self.run_command(commands)
        result = [
            sum(map(float, i)) for i in zip(*[r.split() for r in result])
        ]
        return result

    def compute_dsun(self, wdow_state, skv_path):
        glass_path = os.path.join(WORKDIR, 'obj', 'rm_c_glass_.rad')
        glass_prim = util.parse_primitive(glass_path)
        prim_str = ""
        for i in range(len(wdow_state)):
            for p in glass_prim:
                if p['identifier'].endswith(str(i + 1)):
                    p['modifier'] = wdow_state[i]
                    prim_str += util.put_primitive(p)
        td = tf.mkdtemp()
        temp_env = os.path.join(td, '_env.oct')
        temp_glass = os.path.join(td, '_glass.rad')
        temp_wpi = os.path.join(td, 'wpi')
        temp_ev = os.path.join(td, 'ev')
        with open(temp_glass, 'w') as wtr:
            wtr.write(prim_str)
        sun_rcvr = Receiver('sun', basis='r6')
        _env = "{} {}".format(self.env_black_path, temp_glass)
        Genmtx(sender=self.wpi_sender,
               receiver=sun_rcvr,
               out_path=temp_wpi,
               env=_env,
               opt_path=self.dsopt)
        Genmtx(sender=self.ev_sender,
               receiver=sun_rcvr,
               out_path=temp_ev,
               env=_env,
               opt_path=self.dsopt)
        cmd = "dctimestep {} {} | {} - | getinfo -".format(
            temp_wpi, skv_path, self.to_light)
        wpis = sp.check_output(cmd, shell=True)
        cmd = "dctimestep {} {} | {} - | getinfo -".format(
            temp_ev, skv_path, self.to_light)
        ev = sp.check_output(cmd, shell=True)
        wpi = [float(i) for i in wpis.split()]
        ev = float(ev)
        sp.call('rm -rf {}'.format(td), shell=True)
        return wpi, ev

    def run_command(self, command):
        if self.mp:
            process = mp.Pool(self.wz)
            result = process.map(CHECKOUT, command)
        else:
            result = [CHECKOUT(cmd) for cmd in command]
        return result


if __name__ == "__main__":
    rad_model('setup.cfg')
