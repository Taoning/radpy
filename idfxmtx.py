#!/usr/bin/env python
import os
from math import sin, pi
from argparse import ArgumentParser
import radutil
import vecop
import pdb


class idf_mtx_mult:
    '''
    Given a transmission matrix, modify transmission, reflection,
    and absorption matrices of a BTDF in the format of either
    a IDF or a .xml file
    '''
    # Klems basis: theta and phi division list
    basis_dict = {'145': 'Klems Full',
                  '73': 'Klems Half',
                  '41': 'Klems Quarter',
                  '577': 'Klems Double'}

    abase_list = {"Klems Full": [(0., 1),
                                 (5., 8),
                                 (15., 16),
                                 (25., 20),
                                 (35., 24),
                                 (45., 24),
                                 (55., 24),
                                 (65., 16),
                                 (75., 12),
                                 (90., 0)],

                  "Klems Half": [(0., 1),
                                 (6.5, 8),
                                 (19.5, 12),
                                 (32.5, 16),
                                 (46.5, 20),
                                 (61.5, 12),
                                 (76.5, 4),
                                 (90., 0)],

                  "Klems Quarter": [(0., 1),
                                    (9., 8),
                                    (27., 12),
                                    (46., 12),
                                    (66., 8),
                                    (90., 0)],

                  "Klems Double": [(0., 1),
                                   (5., 16),
                                   (10., 16),
                                   (15., 32),
                                   (20., 32),
                                   (25., 40),
                                   (30., 40),
                                   (35., 48),
                                   (40., 48),
                                   (45., 48),
                                   (50., 48),
                                   (55., 48),
                                   (60., 48),
                                   (65., 32),
                                   (70., 32),
                                   (75., 24),
                                   (86.25, 24),
                                   (90., 0), ]}

    def __init__(self, idf_path, mtx_path, output_path):

        # Do not overwrite existing file
        if os.path.isfile(output_path):
            print("Error: output file exists")
            return

        mtx_data = self.load_mtx(mtx_path)
        idf_sections = radutil.parse_idf(idf_path)
        # assuming model with a single fenestration surface

        try:
            glaz_name = idf_sections['fenestrationsurface:detailed'][0][2]
        except KeyError:
            try:
                glaz_name = idf_sections['window'][0][1]
            except:
                pass

        cfs_sections = idf_sections['construction:complexfenestrationstate']
        cfs_construct = [i for i in cfs_sections if i[0] == glaz_name][0]
        layer_cnt = round((len(cfs_construct) - 9) / 3, 0)  # including gap layer
        fabs1_name = cfs_construct[10]
        babs1_name = cfs_construct[11]
        if layer_cnt == 1:
            abs_list = [fabs1_name]
        elif layer_cnt >= 3:
            fabs2_name = cfs_construct[16]
            babs2_name = cfs_construct[17]
            abs_list = [fabs1_name, babs1_name, fabs2_name, babs2_name]
            if layer_cnt == 5:
                fabs3_name = cfs_construct[22]
                babs3_name = cfs_construct[23]
                abs_list.append(fabs3_name)
                abs_list.append(babs2_name)

        tsol_name = cfs_construct[5]
        tvis_name = cfs_construct[7]
        name_list = [tsol_name, tvis_name] + abs_list
        btdf_sections = idf_sections['matrix:twodimension']
        btdf_s = [s for s in btdf_sections if s[0] in name_list]
        other_btdf = [s for s in btdf_sections if s[0] not in name_list]

        # First write the non-BTDF data into the output idf
        with open(output_path, 'w') as wt:
            for obj_name in idf_sections:
                if obj_name != 'matrix:twodimension' and obj_name != '':
                    for obj in idf_sections[obj_name]:
                        wt.write('%s,\n  %s;\n\n'
                                 % (obj_name, ',\n  '.join(obj)))

            for ob in other_btdf:
                wt.write('matrix:twodimension,\n')
                wt.write('  %s,\n' % ',\n  '.join(ob[:3]))
                col_num = int(ob[2])
                if col_num == 145:
                    ob = self.nest_list(ob[3:], 41)
                    for o in range(len(ob)-1):
                        string = ',    '.join(ob[o])
                        wt.write('  %s,\n' % string)
                    wt.write('  %s;\n\n' % ',    '.join(ob[-1]))
                else:
                    for i in range(3, len(ob[3:]), col_num):
                        row = ', '.join(ob[i: i+col_num])
                        wt.write('  %s,\n' % row)
                    wt.write('  %s;\n\n' % ', '.join(ob[-col_num:]))

        # Go through each part of BTDF (ie tf, tb, rf...)
        for sec in btdf_s:
            name = sec[0]
            row_cnt = int(sec[1])
            col_cnt = int(sec[2])
            if sec[2] not in self.basis_dict:
                result = sec[3:]
            else:
                # Matrix multiplication between BTDF and matrix data
                _btdf_data = self.nest_list(list(map(float, sec[3:])), col_cnt)
                lambdas = self.angle_basis_coeff(self.basis_dict[sec[2]])
                sdata = [list(map(lambda x, y: x * y, i, lambdas))
                         for i in _btdf_data]
                _result = vecop.matmult(sdata, mtx_data)
                _result = [map(lambda x, y: x / y, i, lambdas)
                           for i in _result]
                result = ['%07.5f' % i for b in _result for i in b]  # flatten list

            # Write the new BTDF data to the end of the idf file
            with open(output_path, 'a') as wt:
                header = 'Matrix:TwoDimension,\n'
                header += '  %s,\t!- Name\n' % name
                header += '  %d,\t!- Number of Rows\n' % row_cnt
                header += '  %d' % col_cnt
                wt.write(header)
                nested_result = self.nest_list(result, 41)
                for row in nested_result:
                    string = ',    '.join(row)
                    wt.write(',\n  %s' % string)
                wt.write(';\n\n')

    def load_mtx(self, fpath):
        '''
        Load Radiance produced matrix file into a list of list
        '''
        with open(fpath, 'r') as rd:
            # strip head&tail and split at the empty line
            lines = rd.read().strip().split('\n\n')[1].splitlines()
        nested = []
        for line in lines:
            data_list = line.split()
            sub_list = []
            for i in range(0, len(data_list), 3):
                if data_list[i] != '':
                    sub_list.append(data_list[i])
            sub_list = list(map(float, sub_list))
            nested.append(sub_list)

        return nested

    def angle_basis_coeff(self, basis):
        '''
        Calculate klems basis coefficient
        '''

        def lamb_calc(theta_lr, theta_up, nphi):
            return (0.5 * (sin(pi / 180 * theta_up)**2
                           - sin(pi / 180 * theta_lr)**2) * pi / 180 * 360 / nphi)

        ablist = self.abase_list[basis]
        lambdas = []
        for i in range(len(ablist)-1):
            tu = ablist[i+1][0]
            tl = ablist[i][0]
            np = ablist[i][1]
            lambdas.extend([lamb_calc(tl, tu, np) for n in range(np)])
        return lambdas

    def nest_list(self, li, col_cnt):
        '''
        Make a list of list give the column count
        '''
        nested = []
        for i in range(0, len(li), col_cnt):
            sub_list = []
            for n in range(col_cnt):
                try:
                    sub_list.append(li[i+n])
                except IndexError:
                    break
            nested.append(sub_list)
        return nested


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-ie', help='idf file path', required=True)
    parser.add_argument('-ir', help='Radiance output matrix file path',
                        required=True)
    parser.add_argument('-o', help='output file path', required=True)
    args = parser.parse_args()
    idf_mtx_mult(args.ie, args.ir, args.o)
