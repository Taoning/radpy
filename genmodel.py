#!/usr/bin/env python

"""Can be used to generate Radiance shoebox model."""
#
# Taoning W.

import util
import vecop


class gen_shoe:
    def __init__(self, length, width, height, wthick, sill_hgt, window_hgt, wwr,
                 planum_hgt, ncps_depth, ncps_width, ncps_hgt, ncps_tilt):

        self.wthick = wthick
        # make floor
        self.floor_pts = self.flr_poly(length, width)
        flr_primitive = self.gen_poly_prim('floor', 'floor0', self.floor_pts)
        flr_pstring = util.put_primitive(flr_primitive)
        with open('obj/floor.rad', 'w') as wt:
            wt.write(flr_pstring)
        # make ceiling
        self.ceiling_pts = self.clng_poly(height)
        clng_primitive = self.gen_poly_prim('ceiling', 'ceiling0', self.ceiling_pts)
        clng_pstring = util.put_primitive(clng_primitive)
        with open('obj/ceiling.rad', 'w') as wt:
            wt.write(clng_pstring)
        # make walls
        self.wall_pts = self.walls_poly()
        wall_pstring = ''
        for i in range(len(self.wall_pts)):
            prim = self.gen_poly_prim('wall', 'wall' + str(i), self.wall_pts[i])
            wall_pstring += util.put_primitive(prim)
        with open('obj/wall.rad', 'w') as wt:
            wt.write(wall_pstring)
        # make facade
        facade_poly, self.window_poly = self.facade_poly(sill_hgt,
                                                         window_hgt,
                                                         wwr,
                                                         planum_hgt,
                                                         wthick)

        window_primitive = self.gen_poly_prim('window', 'window0', self.window_poly)
        window_pstring = util.put_primitive(window_primitive)
        facade_pstring = ''
        for i in range(len(facade_poly)):
            facade_primitve = self.gen_poly_prim('facade', 'facade'+str(i), facade_poly[i])
            facade_pstring += util.put_primitive(facade_primitve)
        with open('obj/facade.rad', 'w') as wt:
            wt.write(facade_pstring)
        with open('obj/window.rad', 'w') as wt:
            wt.write(window_pstring)
        # make ncp shade
        shade_poly = self.shade_poly(ncps_depth, ncps_width, ncps_hgt, angle=ncps_tilt)
        shade_prim = self.gen_poly_prim('ncp_shade', 'ncp_shade0', shade_poly)
        shade_pstring = util.put_primitive(shade_prim)
        with open('obj/shade.rad', 'w') as wt:
            wt.write(shade_pstring)

        # self.write_material()

    def write_material(self):
        real_lines = [l for l in self.mat_lines if l[0] != '#']
        with open('obj/materials.rad', 'w') as wt:
            for line in real_lines:
                line_li = line.rstrip().split(',')
                modifier = line_li[0]
                mtype = line_li[1]
                identifier = line_li[2]
                mproperty = line_li[3]
                if mtype == 'plastic':
                    mproperty = float(mproperty)
                    mat_string = util.put_primitive(
                        util.plastic_prim(modifier, identifier,
                                          mproperty, 100, 100, 100, 0, 0))

                elif mtype == 'glass':
                    mproperty = float(mproperty)
                    mat_string = util.put_primitive(
                        util.glass_prim(modifier, identifier,
                                        mproperty, mproperty, mproperty))
                elif mtype == 'bsdf':
                    mat_string = util.put_primitive(
                        util.bsdf_prim(modifier, identifier,
                                       mproperty, [0, 1, 0]))
                wt.write(mat_string)

    def gen_poly_prim(self, mod, idt, polygon):
        prim = {'type': 'polygon', 'str_args': '0', 'int_arg': '0'}
        prim['modifier'] = mod
        prim['identifier'] = idt
        real_args = '%s ' % (len(polygon) * 3)
        real_args += ' '.join([' '.join(map(str, p)) for p in polygon])
        prim['real_args'] = real_args
        return prim

    def flr_poly(self, length, width):
        pt1 = [0, 0, 0]
        pt2 = [width, 0, 0]
        pt3 = [width, length, 0]
        pt4 = [0, length, 0]
        return [pt1, pt2, pt3, pt4]

    def clng_poly(self, height):
        vector = [0, 0, height]
        return vecop.pts_translate(vector, self.floor_pts)

    def facade_poly(self, sill, height, wwr, planum, thickness):

        # extend wall to include the planum space
        planum_ext = [0, 0, planum]
        wall_hgt_pts = vecop.pts_translate(planum_ext, self.ceiling_pts[:2])

        # window wall polygon before window
        polygon = [self.floor_pts[0], wall_hgt_pts[0],
                   wall_hgt_pts[1], self.floor_pts[1]]

        wall_width = vecop.p2p_distance(polygon[0], polygon[3])
        wall_hgt = vecop.p2p_distance(polygon[0], polygon[1])
        wall_area = vecop.area(polygon)
        wind_area = wall_area * wwr

        sill = min(sill, (wall_area - wind_area) / wall_width)
        height = min(height, (wall_hgt - sill))
        wind_width = wind_area / height

        wind_pt0_x = round((wall_width - wind_width) / 2, 3)
        wind_pt0_y = self.floor_pts[0][1]
        wind_pt0_z = sill

        wind_pt2_x = wind_pt0_x + wind_width
        wind_pt2_y = self.floor_pts[0][1]
        wind_pt2_z = sill + height

        wind_pt0 = [wind_pt0_x, wind_pt0_y, wind_pt0_z]
        wind_pt1 = [wind_pt0_x, wind_pt0_y, wind_pt2_z]
        wind_pt2 = [wind_pt2_x, wind_pt2_y, wind_pt2_z]
        wind_pt3 = [wind_pt2_x, wind_pt2_y, wind_pt0_z]

        # window polygon
        wind_polygon = [wind_pt0, wind_pt1, wind_pt2, wind_pt3]

        # wall overlap point
        wall_ovlp_pt = [polygon[3][0], wind_pt0_y, wind_pt0_z]

        # wall polygon
        wall_polygon = [polygon[0], polygon[1], polygon[2], wall_ovlp_pt,
                        wind_pt3, wind_pt2, wind_pt1, wind_pt0,
                        wall_ovlp_pt, polygon[3]]

        wall_normal = vecop.unit_normal(wall_polygon[0],
                                        wall_polygon[1],
                                        wall_polygon[2])

        extrusions = [wall_polygon]
        if thickness != 0.0:
            vector = [i * (-1) * thickness for i in wall_normal]
            polygon2 = vecop.pts_translate(vector, wall_polygon)
            extrusions.append(polygon2)
            for i in range(len(wall_polygon)-1):
                extrusions.append([wall_polygon[i], polygon2[i],
                                   polygon2[i + 1], wall_polygon[i + 1]])

            extrusions.append([wall_polygon[-1], polygon2[-1], polygon2[0], wall_polygon[0]])

        return extrusions, wind_polygon

    def walls_poly(self):
        polygons = []
        for i in range(1, len(self.floor_pts) - 1):
            polygons.append([self.floor_pts[i], self.ceiling_pts[i],
                             self.ceiling_pts[i + 1], self.floor_pts[i + 1]])

        polygons.append([self.floor_pts[-1],
                         self.ceiling_pts[-1],
                         self.ceiling_pts[0],
                         self.floor_pts[0]])

        return polygons

    def shade_poly(self, depth, width, height, angle=0):

        pt0 = [0, -self.wthick, height]
        pt1 = [width, -self.wthick, height]
        pt2 = [width, -depth - self.wthick, height]
        pt3 = [0, -depth - self.wthick, height]
        pts = [pt0, pt1, pt2, pt3]
        print(pt2, pt3)
        if angle != 0:
            angle_rad = vecop.deg2rad(angle)
            trans_vec = vecop.pts2vec(pt0, [0., 0., 0.])
            trans_vec_b = vecop.pts2vec([0., 0., 0.], pt0)
            moved = vecop.pts_translate(trans_vec, pts)
            rotate_ax = vecop.unitize(vecop.pts2vec(pt0, pt1))
            moved[2] = vecop.pt_rotation(moved[2], rotate_ax, angle_rad)
            moved[3] = vecop.pt_rotation(moved[3], rotate_ax, angle_rad)
            print(moved)
            pts = vecop.pts_translate(trans_vec_b, moved)
        return pts


# gen_shoe('config/model_properties.txt', 'config/material_properties.txt')
