#!/usr/bin/env python3
"""
Grid points generation.

1. only works on orthogonal polygons so far
2. decrease from 3D to 2D for point inclusion test
3. generate a grid for each given polygon
4. visualize in objview

T.Wang
"""

# TODO(Taoning)
# implement grid generation methods for non-orthogonal planar surface

import argparse
import os
import radutil
import radgeom
import pdb


class pt_inclusion(object):
    """testing whether a point is inside a polygon using winding number algorithm."""

    def __init__(self, polygon_pts):
        """Initialize the polygon."""
        self.pt_cnt = len(polygon_pts)
        polygon_pts.append(polygon_pts[0])
        self.polygon_pts = polygon_pts

    def isLeft(self, pt0, pt1, pt2):
        """Test whether a point is left to a line."""
        return (pt1[0] - pt0[0]) * (pt2[1] - pt0[1]) \
            - (pt2[0] - pt0[0]) * (pt1[1] - pt0[1])

    def test_inside(self, pt):
        """Test if a point is inside the polygon."""
        wn = 0
        for i in range(self.pt_cnt):
            if self.polygon_pts[i][1] <= pt[1]:
                if self.polygon_pts[i + 1][1] > pt[1]:
                    if self.isLeft(self.polygon_pts[i],
                                   self.polygon_pts[i + 1], pt) > 0:
                        wn += 1
            else:
                if self.polygon_pts[i + 1][1] <= pt[1]:
                    if self.isLeft(self.polygon_pts[i],
                                   self.polygon_pts[i + 1], pt) < 0:
                        wn -= 1
        return wn


def gen_grid(fpath, height, spacing, op=False):
    """Generate a grid of points for orthogonal planar surfaces.

    Parameters:
            fpath: file path of radiance primitives
            height: points' distance from the surface in its normal direction
            spacing: distance between the grid points
            visualize: set to True to visualize the resulting grid points
    Output:
            write the point file to pts directory

    """
    primitives = radutil.parse_primitive(fpath)
    polygons = [i for i in primitives if i['type'] == 'polygon']
    grid_list = []
    for pg in polygons:
        name = pg['identifier']
        modifier = pg['modifier']
        polygon = pg['polygon']
        normal = polygon.normal()
        abs_norm = [abs(i) for i in normal.to_list()]
        drop_idx = abs_norm.index(max(abs_norm))
        pg_pts = [i.to_list() for i in polygon.vertices]
        pt_cnt = len(pg_pts)
        plane_height = sum([i[drop_idx] for i in pg_pts]) / pt_cnt
        [i.pop(drop_idx) for i in pg_pts]  # dimension reduction
        _ilist = [i[0] for i in pg_pts]
        _jlist = [i[1] for i in pg_pts]
        imax = max(_ilist)
        imin = min(_ilist)
        jmax = max(_jlist)
        jmin = min(_jlist)
        xlen_spc = ((imax - imin) / spacing)
        ylen_spc = ((jmax - jmin) / spacing)
        xstart = ((xlen_spc - int(xlen_spc) + 1)) * spacing / 2
        ystart = ((ylen_spc - int(ylen_spc) + 1)) * spacing / 2
        x0 = [
            float('%g' % x) + xstart
            for x in radutil.frange_inc(imin, imax, spacing)
        ]
        y0 = [
            float('%g' % x) + ystart
            for x in radutil.frange_inc(jmin, jmax, spacing)
        ]
        raw_pts = [[i, j] for i in x0 for j in y0]
        if polygon.normal() == radgeom.Vector(0, 0, 1):
            pt_incls = pt_inclusion(pg_pts)
        else:
            pt_incls = pt_inclusion(pg_pts[::-1])
        _grid = [p for p in raw_pts if pt_incls.test_inside(p) > 0]
        if op:
            grid_dir = normal.reverse()
        else:
            grid_dir = normal
        p_height = sum([height * i for i in grid_dir.to_list()]) + plane_height
        grid = []
        _idx = list(range(3))
        _idx.pop(drop_idx)
        for g in _grid:
            tup = [0.0] * 3 + grid_dir.to_list()
            tup[drop_idx] = p_height
            tup[_idx[0]] = g[0]
            tup[_idx[1]] = g[1]
            grid.append(tup)

        grid_list.append(grid)

    output = '\n'.join([
        '\n'.join([' '.join(map(str, row)) for row in gl]) for gl in grid_list
    ])

    return output


def triangulate(points):
    """Triangulate a planar convex polygon.

    parameters: list of points of a polygon
    return: list of triangles

    """
    pt_cnt = len(points)
    if pt_cnt > 3:
        triangles = []
        i = 1
        while i < (pt_cnt - 1):
            end_idx = min(i + 2, pt_cnt)
            tri = [points[0]] + points[i:end_idx]
            triangles.append(tri)
            i += 1
    else:
        triangles = [points]
    return triangles


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i',
                        '--input_fpath',
                        type=str,
                        required=True,
                        help='input file')
    parser.add_argument('-hgt',
                        '--distance_from_plane',
                        type=float,
                        help='distance from plane/polygon in normal direction')
    parser.add_argument('-s', '--spacing', type=float, help='grid spacing')
    parser.add_argument('-vis',
                        action='store_true',
                        help='generate a rad file that can be visualized \
                              with Radiance objview program')
    parser.add_argument(
        '-op',
        action='store_true',
        help='set sampling direction opposite to surface normal')
    args = parser.parse_args()
    grids = gen_grid(args.input_fpath,
                     args.distance_from_plane,
                     args.spacing,
                     op=args.op)
    print(grids)
    if args.vis:
        vis_path = 'tmpgrid.rad'
        psize = round(args.spacing / 3, 3)
        tup = [0] * 4
        tup[-1] = psize
        with open(vis_path, 'w') as wt:
            wt.write('void plastic dot 0 0 5 .6 .1 .1 0 0\n')
            for g in grids.splitlines():
                pt = "dot sphere sphere_1 0 0 4 {} {}\n".format(
                    ' '.join(g.split(' ')[:3]), psize)
                wt.write(pt)
        cmd = "objview -s {}".format(vis_path)
        os.system(cmd)
        os.remove(vis_path)
