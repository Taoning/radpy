#!/usr/bin/env python3
# Some functions requires python 3.X
#
# T.Wang

import subprocess as sp
import pdb
from PIL import Image, ImageStat
import math


def exam_high(fpath, detect=True):
    '''
    Detect saturated pixels
    Parameters:
    fpath: file path to the image
    detect: set to False to output an image with saturated pixels marked
    '''

    im = Image.open(fpath, 'r')
    stat = ImageStat.Stat(im)
    high = [i[1] for i in stat.extrema]
    print('Brightest pixel: {}'.format(high))
    if sum(high) == 255 * 3:
        return True
    else:
        return False


def hdrgen(ldrpath, outpath, rsp, opt):
    cmd = "hdrgen {} -r {} -o {} {}".format(opt, rsp, outpath, ldrpath)
    sp.call(cmd, shell=True)


def get_imgdim(inp_hdr):
    '''
    Get HDR image dimensions
    '''
    cmd = 'getinfo -d {}'.format(inp_hdr)
    stdout = sp.getoutput(cmd).split()
    xres = int(stdout[4])
    yres = int(stdout[2])
    return xres, yres


def fractionize(inp_hdr, vfrac):
    '''
    Calculate the new image resolutions based on fraction factor
    positional args: raw_hdr, vfrac
    '''
    xres, yres = get_imgdim(inp_hdr)
    new_res = math.floor(vfrac * yres + .5)
    return new_res


def crop2square(inp_hdr, out_hdr, res, x_offset=0.0, y_offset=0.0):
    '''
    Crop the image to a square format
    position args: hdr, squared_hdr, xres, yres
    optional args: x_offset=0.0, y_offset=0.0
    '''

    xcent = math.floor(res / 2) - x_offset
    ycent = math.floor(res / 2) - y_offset
    cmd = 'ra_xyze -r -o {} '.format(inp_hdr)
    cmd += '| pcompos -x {} -y {} \=00 - '.format(res, res)
    cmd += '{} {} > {}'.format(xcent, ycent, out_hdr)
    print(cmd)
    sp.call(cmd, shell=True)


def proj_corr(inp_hdr, out_hdr, cal_file, view_line):
    '''
    Image projection transformation
    positional args: inp_hdr, out_hdr, cal_file, view_line
    '''
    cmd = 'pcomb -f {} -o {} '.format(cal_file, inp_hdr)
    cmd += '| getinfo -a "VIEW={}" > {}'.format(view_line, out_hdr)
    print(cmd)
    sp.call(cmd, shell=True)


def vignett_corr(inp_hdr, out_hdr, corr_line, xoff=0.0, yoff=0.0):
    '''
    Applying vignetting correction to a HDR image
    positional args: inp_hdr, out_hdr, corr_line
    optional args: xoff=0.0, yoff=0.0
    '''

    cmd = "pcomb -e 'sq(x):x*x' "
    cmd += "-e 'td(x):tan(PI*x/180)' "
    cmd += "-e 'lens_corr(deg) : {}' ".format(corr_line)
    cmd += "-e 'centx=xmax/2+{} ; centy=ymax/2+{}' ".format(xoff, yoff)
    cmd += "-e 'xne=(x-centx)/(ymax/2); yne=(y-centy)/(ymax/2)' "
    cmd += "-e 'deg_cent=90*sqrt(sq(xne) + sq(yne))' "
    cmd += "-e 'corr=1.0/lens_corr(deg_cent)' "
    cmd += "-e 'ro=corr*ri(1);go=corr*gi(1);bo=corr*bi(1)' "
    cmd += "-o {} | ra_rgbe -r - {}".format(inp_hdr, out_hdr)
    print(cmd)
    sp.call(cmd, shell=True)


def hdr_illum(inp_hdr):
    '''
    Calculate illuminance from a HDR image
    assuming hemispherical projection
    '''
    cmd = "pcomb -e 'sq(x):x*x' -e 'ar=PI/2*sqrt(sq(2/xmax*x-1)+sq(2/ymax*y-1))' "
    cmd += "-e 'cf=WE*sq(PI*2/(xmax+ymax))*if(ar-PI/2,0,if(ar-.01,cos(ar)*sin(ar)/ar,1))' "
    cmd += "-e 'lo=cf*li(1)' -o {} | pvalue -h -H -pG -df | total -if".format(
        inp_hdr)
    return float(sp.run(cmd, shell=True, check=True, stdout=sp.PIPE).stdout)


def clean_circle(inp_hdr, out_hdr):
    '''
    Blacken the pixels outside the circle
    positional args: inp_hdr, out_hdr
    '''
    cmd = "pcomb -e 'inside(x,y):if(sqrt((x-xmax/2)^2+(y-ymax/2)^2)-xmax/2,0,1)' "
    cmd += "-e 'ro=if(inside(x,y),ri(1),0);go=if(inside(x,y),gi(1),0); bo=if(inside(x,y),bi(1),0)' "
    cmd += "-o {} > {}".format(inp_hdr, out_hdr)
    sp.call(cmd)


def scale_hdr(inp_hdr, out_hdr, factor):
    '''
    Scale the hdr image (energy) by factor, convert image to Radiance run-length encoding file
    positional args: inp_hdr, out_hdr, factor
    '''
    cmd = "pcomb -s {} -o {} | ra_rgbe -r > {}".format(factor, inp_hdr,
                                                       out_hdr)
    sp.call(cmd, shell=True)


def transform_hdr(inp_hdr, out_hdr, x, y):

    cmd = 'pfilt -1 -e 1 -x {} -y {} {} > {}'.format(x, y, inp_hdr, out_hdr)
    sp.call(cmd, shell=True)


def max_pos(inp_hdr):
    '''
    Get the position of the beightest pixel
    '''
    cmd = 'pextrem -o {}'.format(inp_hdr)
    stdout = sp.getoutput(cmd)
    local = stdout.splitlines()[1].split()[:2]
    return local


def calc_DHI(inp_hdr, sunloc_x, sunloc_y):
    '''
    Calculate diffuse horizontal irradiance by masking the sun
    '''
    xres, yres = get_imgdim(inp_hdr)
    radius = math.floor(2.5 / 180 * xres + 0.5)
    cmd = 'pcomb -x {} -y {} '.format(xres, yres)
    cmd += "-e 'ro=b;go=b;bo=b;b=if((x-{})^2+(y-{})^2-{}^2,0,1)'".format(
        sunloc_x, sunloc_y, radius)
    cmd += "> dot.hdr"
    sp.call(cmd, shell=True)
    cmd = "pcomb -e 'ro=if(li(1)-.5,ri(1),ri(2));go=if(li(1)-.5,gi(1),gi(2));bo=if(li(1)-.5,bi(1),bi(2))' "
    cmd += "-o ./dot.hdr -o {} > ./masked.hdr".format(inp_hdr)
    sp.call(cmd, shell=True)
    img_illum = hdr_illum("./masked.hdr")
    sp.call('rm ./dot.hdr ./masked.hdr', shell=True)
    return img_illum


def circle_crop(hdr_path, xpos, ypos, radii):
    pcomb_cmd = "pcomb -e 'sq(x):x*x' "
    pcomb_cmd += "-e 'ro=if(sq(x-{})+sq(y-{})-sq({}),0,ri(1));".format(
        xpos, ypos, radii)
    pcomb_cmd += "go=if(sq(x-{})+sq(y-{})-sq({}),0,gi(1));".format(
        xpos, ypos, radii)
    pcomb_cmd += "bo=if(sq(x-{})+sq(y-{})-sq({}),0,bi(1))' -o {}".format(
        xpos, ypos, radii, hdr_path)
    return pcomb_cmd


def get_lum(hdrpath, xpos, ypos, radi):
    """."""
    ccoef = (0.2126, 0.7152, 0.0722)  # sRGB
    ccoef = (0.265, 0.67, 0.065)  # Radiance RGB
    pcomb_cmd = "pcomb -e 'sq(x):x*x' "
    pcomb_cmd += "-e 'ro=if(sq(x-{})+sq(y-{})-sq({}),0,ri(1));".format(
        xpos, ypos, radi)
    pcomb_cmd += "go=if(sq(x-{})+sq(y-{})-sq({}),0,gi(1));".format(
        xpos, ypos, radi)
    pcomb_cmd += "bo=if(sq(x-{})+sq(y-{})-sq({}),0,bi(1))' -o {}".format(
        xpos, ypos, radi, hdrpath)
    pvalue_cmd = "pvalue -o -u -h -H -d "
    rcalc_cmd = "rcalc -e '$1=179.*({}*$1+{}*$2+{}*$3)' ".format(*ccoef)
    cmd = "{} | {} | {}".format(pcomb_cmd, pvalue_cmd, rcalc_cmd)
    results = [
        float(i) for i in sp.check_output(cmd, shell=True).split() if i != b'0'
    ]
    avg_lum = sum(results) / len(results)
    return avg_lum


def pextrem(path):
    cmd = "pextrem {}".format(path)
    result = sp.check_output(cmd, shell=True)
    return result
