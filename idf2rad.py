#!/usr/bin/env python
"""Simplified conversion from IDF to Radiance compatiable format.

Material definition works when visible properties are defined
in IDF file, user must check the output material file and modify accordingly

Taoning.W.

TODO: rotate the zones
TODO: window divider conversion (maybe...)
"""


import argparse
import os
import radutil
import vecop


class idf2rad(object):
    """Convert a IDF file to ready to use Radiance files."""

    def __init__(self, idf_fpath, out_dir):
        """Initialize the class."""
        self.out_dir = out_dir

        surfaces, fene_srfs, \
            self.mat_dict, self.constrct_dict = self.parse_idf(idf_fpath)

        self.srf_prims = self.get_srf_prim(surfaces)
        self.fsrf_prims = self.get_fsrf_prim(fene_srfs)

        # Clipping wall surfaces with window surfaces
        [self.mod_fsrf_host(f) for f in self.fsrf_prims]

        self.mat_prims = self.get_material_prim()
        geo_string, mat_string = self.prim2string()
        self.write2file(geo_string, mat_string)

    def write2file(self, geo_str, mat_str):
        """Write to a file."""
        with open(os.path.join(self.out_dir, 'mat.rad'), 'w') as wtr:
            wtr.write(mat_str)

        with open(os.path.join(self.out_dir, 'geom.rad'), 'w') as wtr:
            wtr.write(geo_str)

    def parse_idf(self, idf_path):
        """Parse the IDF."""
        idf_dict = radutil.parse_idf(idf_path)

        # Building general information
        building = idf_dict['building'][0]
        self.building_dict = {building[0]: building[1:]}
        self.building_orient = float(self.building_dict.values()[0][0])

        # Surfaces and windows
        surfaces = idf_dict['buildingsurface:detailed']
        fene_srfs = idf_dict['fenestrationsurface:detailed']

        # Zones
        zones = idf_dict['zone']
        zone_dict = {}
        for z in zones:
            zone_dict[z[0]] = z[1:]

        # Materials
        materials = self.search_1st_kwd(idf_dict, 'material')
        material_dict = {}
        for mat in materials:
            material_dict[mat[0]] = mat[1:]

        # Constructions
        constructions = self.search_1st_kwd(idf_dict, 'construction')
        construct_dict = {}
        for const in constructions:
            construct_dict[const[0]] = const[1:]

        return surfaces, fene_srfs, material_dict, construct_dict

    def get_srf_prim(self, surfaces):
        """Get the surface primitives."""
        srf_prims = {}

        for srf in surfaces:
            prim = {'type': 'polygon', 'str_args': '0', 'int_arg': '0'}
            name = srf[0]
            srf_type = srf[1]
            srf_construct = srf[2]
            pt_num = int(srf[9])
            pts = ' '.join(srf[10:10 + pt_num * 3])
            prim['modifier'] = srf_construct
            prim['identifier'] = name
            prim['real_args'] = '%d ' % (pt_num * 3)
            prim['real_args'] += pts
            srf_prims[name] = prim

        return srf_prims

    def get_fsrf_prim(self, fene_srfs):
        """Get the fenestration surface primitives."""
        fsrf_prims = []

        for fsrf in fene_srfs:
            prim = {'type': 'polygon', 'str_args': '0', 'int_arg': '0'}
            name = fsrf[0]
            srf_type = fsrf[1]
            srf_construct = fsrf[2]
            host = fsrf[3]
            pt_num = fsrf[9]
            pts = ' '.join(fsrf[10:])
            prim['modifier'] = srf_construct
            prim['identifier'] = name
            prim['real_args'] = '%d ' % (int(pt_num) * 3)
            prim['real_args'] += pts
            prim['host'] = host
            fsrf_prims.append(prim)

        return fsrf_prims

    def mod_fsrf_host(self, fsrf):
        """Clipping wall surfaces with their window surfaces.

        Only works on four sided walls and windows.
        """
        host_prim = self.srf_prims[fsrf['host']]
        fpts = radutil.pt_coord(fsrf)
        spts = radutil.pt_coord(host_prim)

        # sort window surface's vertices to sync with it host's (imperfect)
        _fpts = []
        for p in fpts:
            dists = [vecop.p2p_distance(p, i)
                     for i in [spts[n] for n in [0, -3, -2, -1]]]
            _fpts.append(fpts[dists.index(sorted(dists)[0])])

        _special = vecop.ptline_proj(spts[0], spts[1], _fpts[0])
        polygon = [spts[0], _special, _fpts[0]]
        polygon.extend(_fpts[::-1])
        polygon.append(_special)
        polygon.extend(spts[1:])

        real_args = '%d ' % (len(polygon) * 3)
        real_args += ' '.join([' '.join(map(str, p)) for p in polygon])
        self.srf_prims[fsrf['host']]['real_args'] = real_args

    def get_material_prim(self):
        """Get material primitives from geometry primitives."""
        mat_prims = []

        for p in self.srf_prims.values():
            # Assuming last material data entry to be visible absorbtance
            vlr = 1 - float(self.mat_dict[
                self.constrct_dict[p['modifier']][-1]][-1])
            mat_prims.append(radutil.plastic_prim('void', p['modifier'],
                                               vlr, 100, 100, 100, 0, 0))

        for p in self.fsrf_prims:
            try:
                tvis = self.mat_dict[self.constrct_dict[p['modifier']][-1]][-1]
                if tvis != '':
                    tvis = float(tvis)
                else:
                    print("Undefined Tvis for %s, assuming 0.6" % p['modifier'])
                    tvis = 0.6
            except KeyError:
                # if self.constrct_dict[p['modifier']][0] == 'LBNLWindow':
                #     print('%s as a CFS' % p['modifier'])
                #     tfvis = self.constrct_dict[p['modifier']][6]
                #     tvis_sec = [i for i in idf_dict['matrix:twodimension']\
                #       if i[0] == tfvis][0]
                #     tvis = self.cfs_tvis(tvis_sec) * float(tvis_sec[3])
                # else:
                #     print('Unknow Tvis for %s, assuming 0.6;' % p['modifier'])
                #     tvis = 0.6
                tvis = 0.6
            mat_prims.append(radutil.glass_prim('void', p['modifier'], tvis, tvis, tvis))

        return mat_prims

    def prim2string(self):
        """Put primitives to strings."""
        geo_string = ''
        mat_string = ''
        srf_prims = self.rm_dup_prim(self.srf_prims.values())
        fsrf_prims = self.rm_dup_prim(self.fsrf_prims)
        mat_prims = self.rm_dup_prim(self.mat_prims)

        for p in srf_prims:
            geo_string += radutil.put_primitive(p)

        for p in fsrf_prims:
            geo_string += radutil.put_primitive(p)

        for prim in mat_prims:
            mat_string += radutil.put_primitive(prim)

        return geo_string, mat_string

    def cfs_tvis(self, sec):
        """Center of glass transmission coversion factor for BTDF."""
        basis = radutil.abase_list[radutil.basis_dict[sec[2]]]
        coeff = radutil.lambda_calc(basis[0][0],  # lower theta
                                 basis[1][0],  # upper theta
                                 basis[0][1])  # nphis
        return coeff

    def rm_dup_prim(self, prims):
        """Remove duplicated prims from the list."""
        return [dict(tupleized)
                for tupleized in set(tuple(item.items())
                                     for item in prims)]

    def search_1st_kwd(self, dictionary, keyword, sep=':'):
        """Search in dictionary if any key's first part contains the keyword."""
        search = [dictionary[n] for n in
                  [i for i in dictionary if keyword in i.split(sep)[0]]]
        flatten = [a for b in search for a in b]

        return flatten


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', help='IDF file path', required=True)
    parser.add_argument('-o', default='./', help='Output directory')

    args = parser.parse_args()
    idf_path = args.i
    out_dir = args.o

    idf2rad(idf_path, out_dir)
