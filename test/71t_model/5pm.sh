#!/bin/bash
#
# 71t 5-phase methods image-based
#
#


NPROC=8

PVCNT=1

PVOPTS="-ab 1 -ad 10 -lw 1e-2 -c ${PVCNT}"

PVDOPTS="-i -ab 1 -ad 10 -lw 1e-2 -c ${PVCNT}"

DOPTS="-ab 1 -ad 100 -lw 1e-5 -c ${PVCNT}"

DDOPTS="-ab 0 -c 500"

CDROPTS="-ffc -i -ab 1 -ad 10 -dc 1 -dt 0 -dj 0"

CDFOPTS="-ffc -ab 1 -ad 10 -dc 1 -dt 0 -dj 0"

RES=32

VIEW_FILE='view/rm_c_south.vf'

MODEL='./model/71t.rad'

BLACK_MODEL='./model/71t_black.rad'

## V matrix##
vmx_cmd="vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -pj .7 -c ${PVCNT} -ff \
| rfluxmtx -v -ffc `vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -d` \
-o matrices/rm_c_vmx/%03d.hdr ${PVOPTS} -n ${NPROC} - \
port/rm_c_glass_port.rad ${MODEL}"
eval ${vmx_cmd}

## Vd matrix##
vmxd_cmd="vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -pj .7 -c ${PVCNT} -ff \
| rfluxmtx -v -ffc `vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -d` \
-o matrices/rm_c_vmx_vd/%03d.hdr ${PVDOPTS} -n ${NPROC} - \
./port/rm_c_glass_port.rad ${BLACK_MODEL}"
eval ${vmxd_cmd}

## D matrix##
dmx_cmd="rfluxmtx -v -ff ${DOPTS} -n ${NPROC} ./port/rm_c_glass_port.rad \
./port/skyglow.rad ${MODEL} > matrices/rm_c_glass.dmx"
eval ${dmx_cmd}

## Dd matrix##
dmxd_cmd="rfluxmtx -v -ff ${DDOPTS} -n ${NPROC} ./port/rm_c_glass_port.rad \
./port/skyglow.rad ${BLACK_MODEL} > matrices/rm_c_glass_d.dmx"
eval ${dmxd_cmd}

cnt 5186 | rcalc -o 'sol${recno}' > solnames.txt
suns_cmd="cnt 5186 | rcalc -e MF:6 -f reinsrc.cal -e Rbin=recno \
-o 'void light sol\${recno} 0 0 3 1e6 1e6 1e6 sol\${recno} source \
sun 0 0 4 \${ Dx } \${ Dy } \${ Dz } 0.533' > ./port/suns.rad"
eval ${suns_cmd}

## Direct sun + tensor tree bsdf octree
oconv hr_bsdf/shade_g6_.rad model/71t_black.rad ./port/suns.rad > oct/sun_g6.oct

## Cdr ##
sunr_cmd="vwrays -ff -vf ${VIEW_FILE} -x ${RES} -y ${RES} \
| rcontrib ${CDROPTS} -n ${NPROC} `vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -d` \
-o matrices/sunr/%04d.hdr -M ./solnames.txt oct/sun_g6.oct"
eval ${sunr_cmd}

## Cdf ##
sunf_cmd="vwrays -ff -vf ${VIEW_FILE} -x ${RES} -y ${RES} \
| rcontrib ${CDFOPTS} -n ${NPROC} `vwrays -vf ${VIEW_FILE} -x ${RES} -y ${RES} -d` \
-o matrices/sunf/%04d.hdr -M ./solnames.txt oct/sun_g6.oct"
eval ${sunf_cmd}

## map1 ##
oconv ${MODEL} port/rm_c_glass_port.rad > oct/rm_c_reflmap_M1.oct
map1_cmd="rpict -x ${RES} -y ${RES} -ps 1 -av 0.31831 0.31831 0.31831 -ab 0 \
-vf ${VIEW_FILE} oct/rm_c_reflmap_M1.oct > matrices/rm_c_reflmap_M1.hdr"
eval ${map1_cmd}

## map2 ##
oconv ${MODEL} port/rm_c_glass_port_black.rad > oct/rm_c_reflmap_M2.oct
map2_cmd="rpict -x ${RES} -y ${RES} -ps 1 -av 0.31831 0.31831 0.31831 -ab 0 \
-vf ${VIEW_FILE} oct/rm_c_reflmap_M2.oct > matrices/rm_c_reflmap_M2.hdr"
eval ${map2_cmd}

## 3PM direct part -- convert illum to lum ##
for img in matrices/rm_c_vmx_vd/*.hdr; do
  img_b=$(basename ${img} .hdr)
  pcomb -h -e 'ro=ri(1)*ri(2);go=gi(1)*gi(2);bo=bi(1)*bi(2)' \
        -o matrices/rm_c_reflmap_M1.hdr -o ${img} \
        > matrices/rm_c_vmx_d/${img_b}.hdr
done

## Direct sun part, converting illum to lum and add facade part
for img in matrices/sunr/*.hdr; do
  img_b=$(basename ${img} .hdr)
  pcomb -h -e 'ro=ri(1)*ri(2)+ri(3);go=gi(1)*gi(2)+gi(3);bo=bi(1)*bi(2)+bi(3)' \
  -o matrices/rm_c_reflmap_M2.hdr -o ${img} -o matrices/sunf/${img_b}.hdr \
  > matrices/sun/${img_b}.hdr
done

## Multiplying matrices for each parts
dctimestep -o result/3pm/%03d.hdr matrices/rm_c_vmx/%03d.hdr ./bsdf/back_kf.xml matrices/rm_c_glass.dmx sky/71t_012018_m1.smx
dctimestep -o result/3pm_d/%03d.hdr matrices/rm_c_vmx_d/%03d.hdr ./bsdf/back_kf.xml matrices/rm_c_glass_d.dmx sky/71t_012018_m1_d.smx
dctimestep -o result/sun/%03d.hdr matrices/sun/%04d.hdr sky/71t_012018_m6_d.smx

## Combine images for final results
for img in result/3pm/*.hdr; do
  img_b=$(basename ${img} .hdr)
  pcomb -h -e 'ro=ri(1)-ri(2)+ri(3);go=gi(1)-gi(2)+gi(3);bo=bi(1)-bi(2)+bi(3)' \
  -o ${img} -o result/3pm_d/${img_b}.hdr -o result/sun/${img_b}.hdr \
  > result/5pm/${img_b}.hdr
done

