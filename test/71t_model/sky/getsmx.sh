#!/bin/bash
#
# generate daylight matrices for 5PM
#
#
WEA='oak.wea'
WEA_NAME=`basename ${WEA} .wea`
MF=1

gendaymtx -m ${MF} -of ${WEA} > ${WEA_NAME}_m${MF}.smx
gendaymtx -m ${MF} -of -d ${WEA} > ${WEA_NAME}_m${MF}_d.smx
gendaymtx -m 6 -5 .533 -d -of ${WEA} > ${WEA_NAME}_m6_d.smx
