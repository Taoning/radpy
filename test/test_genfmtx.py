import genfmtx
import unittest
import subprocess as sp
import radutil
import os

class TestGenfmtx(unittest.TestCase):
    window_path = 'window.rad'
    ncps_path = 'awning.rad'
    env_path = 'material.rad south_wall.rad {}'.format(ncps_path)
    window_prim = radutil.parse_primitive(window_path)
    ncps_prim = radutil.parse_primitive(ncps_path)

    def test_kf_xml(self):
        result_name = "test_kf.xml"
        radutil.silent_remove(result_name)
        genfmtx.Genfmtx(win_prim=self.window_prim,
                        ncs_prim=self.ncps_prim,
                        out_path=result_name,
                        wrap=True,
                        env=self.env_path,
                        opt='-n 4 -ab 0 -ff',
                       )
        self.assertTrue(os.path.isfile(result_name))

    def test_cmd_line(self):
        result_name = "test_cmd.xml"
        radutil.silent_remove(result_name)
        cmd = "genfmtx.py -wf {} -sf {} -ss kf -rs kf ".format(
            self.window_path, self.ncps_path)
        cmd += f"-o {result_name} -opt '-ab 0 -ff' -wrap "
        cmd += "-env {}".format(self.env_path)
        sp.call(cmd, shell=True)
        self.assertTrue(os.path.isfile(result_name))

    def test_ttree(self):
        result_name = "test_tt.xml"
        radutil.silent_remove(result_name)
        genfmtx.Genfmtx(win_prim=self.window_prim,
                        ncs_prim=self.ncps_prim,
                        out_path=result_name,
                        ss='sc32',rs='sc32',
                        opt='-n 4 -ab 0 -ff',
                        env=self.env_path,
                        ttree=True, wrap=True)
        self.assertTrue(os.path.isfile(result_name))

    def test_method2(self):
        result_name = "test_method2.mtx"
        radutil.silent_remove(result_name)
        genfmtx.Genfmtx(win_prim=self.window_prim,
                        scale=1.1, depth=3,
                        out_path=result_name,
                        ss='kf', rs='kf',
                        env=self.env_path,
                        opt='-ab 0')
        self.assertTrue(os.path.isfile("test_method2_tf.mtx"))

    def test_refl(self):
        result_name = "test_refl.xml"
        radutil.silent_remove(result_name)
        genfmtx.Genfmtx(win_prim=self.window_prim,
                        ncs_prim=self.ncps_prim,
                        out_path=result_name,
                        env=self.env_path,
                        opt='-ab 1',
                        refl=True, wrap=True)
        self.assertTrue(os.path.isfile(result_name))


if __name__ == "__main__":
    unittest.main()

