
import unittest
import util


class TestUtilFunction(unittest.TestCase):
    def test_parse_vu(self):
        vf = "viewfile.vf"
        view_dict = util.parse_vu(vf)
        answer = {'vt':'a','vd':[0,1,0],'vp':[0,0,0],
                  'vv':180, 'vh':180, 'vu':[0,0,1]}
        self.assertEqual(view_dict, answer)

    def test_parse_opt(self):
        opt = "optfile.opt"
        with open(opt) as rdr: opt_str = rdr.read()
        opt_dict = util.parse_opt(opt_str)
        answer = {'ab':3, 'ad':1000, 'lw':0.00001, 'I':''}
        self.assertEqual(opt_dict, answer)

if __name__ == "__main__":
    unittest.main()

