import math
import geom
import unittest

class TestGeomMethods(unittest.TestCase):
    def test_convexhull(self):
        p1 = geom.Point(0, 0, 0)
        p2 = geom.Point(1, 1, 0)
        p3 = geom.Point(1, 1, 1)
        p4 = geom.Point(0, 0, 1)
        pts = [p1, p2, p3, p4]
        hull = geom.Convexhull(pts, geom.Vector(math.sqrt(2),-math.sqrt(2),0))
        answer = [p2.tolist(), p3.tolist(), p4.tolist(), p1.tolist()]
        self.assertEqual([i.tolist() for i in hull.hull], answer) 


