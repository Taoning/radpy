import genmtx
import unittest
import radutil
import pdb

class TestGenmtxFunction(unittest.TestCase):
    def test_sender_view_path(self):
        vf = "71t_model/view/rm_c_south.vf"
        sndr = genmtx.Sender(vf, xres=800, yres=800, ray_cnt=3)
        self.assertTrue(sndr.form=='view')

    def test_sender_grid_path(self):
        gpath = "71t_model/pts/wpc.pts"
        sndr = genmtx.Sender(gpath)
        self.assertTrue(sndr.form=='grid')

    def test_sender_srf_path(self):
        spath = "71t_model/obj/rm_c_floor.rad"
        sndr = genmtx.Sender(spath, basis='kf')
        self.assertTrue(sndr.form=='srf')

    def test_sender_view_dict(self):
        _view = {'vt':'a','vp':[0,0,0],'vd':[0,-1,0]}
        sndr = genmtx.Sender(_view, xres=800, yres=800, ray_cnt=3)
        self.assertTrue(sndr.form=='view')

    def test_sender_srf_dict(self):
        prim_path = '71t_model/obj/rm_c_floor.rad'
        _prim = radutil.parse_primitive(prim_path)
        sndr = genmtx.Sender(_prim[0], basis='kf')
        self.assertTrue(sndr.form=='srf')

    def test_sender_srf_list(self):
        prim_path = '71t_model/obj/rm_c_floor.rad'
        _prim = radutil.parse_primitive(prim_path)
        sndr = genmtx.Sender(_prim, basis='kf')
        self.assertTrue(sndr.form=='srf')

    def test_sender_grid_list(self):
        gpath = "71t_model/pts/wpc.pts"
        with open(gpath) as rdr:
            raw = rdr.read()
            raw = [i.split() for i in raw]
        sndr = genmtx.Sender(raw)
        self.assertTrue(sndr.form=='grid')

if __name__ == "__main__":
    unittest.main()

