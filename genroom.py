import geom
import util
import pdb

class Room(object):
    """Make a shoebox."""

    def __init__(self, width, depth, height, origin=geom.Point()):
        self.width = width
        self.depth = depth
        self.height = height
        self.origin = origin
        flr_pt2 = origin + geom.Vector(width, 0, 0)
        flr_pt3 = flr_pt2 + geom.Vector(0, depth, 0)
        self.floor = geom.Rectangle3P(origin, flr_pt2, flr_pt3)
        extrusion = self.floor.extrude(geom.Vector(0, 0, height))
        self.clng = extrusion[1]
        self.swall = Wall(extrusion[2], 'swall')
        self.ewall = Wall(extrusion[3], 'ewall')
        self.nwall = Wall(extrusion[4], 'nwall')
        self.wwall = Wall(extrusion[5], 'wwall')

    def for_real(self):
        self.clg_real = self.clng.to_real()
        self.flr_real = self.floor.to_real()
        self.swall_real = self.swall.to_real()
        self.nwall_real = self.nwall.to_real()
        self.ewall_real = self.ewall.to_real()
        self.wwall_real = self.wwall.to_real()

    def to_prim(self):
        clg_prim = {}
        clg_prim['type'] = 'polygon'

class Wall(object):
    """Room wall object."""

    def __init__(self, polygon, name):
        self.centroid = polygon.centroid()
        self.polygon = polygon
        self.vertices = polygon.vertices
        self.vect1 = (self.vertices[1] - self.vertices[0]).unitize()
        self.vect2 = (self.vertices[2] - self.vertices[1]).unitize()
        self.name = name

    def make_window(self, dist_left, dist_bot, width, height, wwr=None):
        if wwr is not None:
            assert type(wwr) == float, 'WWR must be float'
            win_polygon = self.polygon.scale(geom.Vector(*[wwr]*3), self.centroid)
        else:
            win_pt1 = self.vertices[0]\
                    + self.vect1.scale(dist_bot)\
                    + self.vect2.scale(dist_left)
            win_pt2 = win_pt1 + self.vect1.scale(height)
            win_pt3 = win_pt1 + self.vect2.scale(width)
            win_polygon = geom.Rectangle3P(win_pt3, win_pt1, win_pt2)
        return win_polygon

    def add_window(self, window_polygon):
        self.polygon = self.polygon - window_polygon

    def facadize(self, thickness):
        self.facade = self.polygon.extrude(self.polygon.normal().scale(thickness))

    def to_prim(self):
        prims = []
        prim = {'modifier':'wall', 'type':'polygon',
                'str_args':'0', 'int_arg':'0'}
        if hasattr(self, 'facade'):
            for idx in range(len(self.facade)):
                _prim = prim.copy()
                _prim['identifier'] = "{}_{}".format(self.name, idx)
                _prim['real_args'] = self.facade[idx].to_real()
                prims.append(_prim)
        else:
            _prim = prim.copy()
            _prim['identifier'] = self.name
            _prim['real_args'] = self.polygon.to_real()
            prims.append(_prim)
        return prims


rm1 = Room(10, 10, 3)
swall = rm1.swall
swin = swall.make_window(1, 1, 1, 1)
swall.add_window(swin)
swall.facadize(.2)
swall_prims = swall.to_prim()
prim_str = [util.put_primitive(i) for i in swall_prims]
with open('test.rad','w') as wtr:
    [wtr.write(i) for i in prim_str]
pdb.set_trace()
